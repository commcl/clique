// for iOS add cross block like overlay
// Because the click event doesn't work for the body element
// if there are any ::before or ::after elements above

(function($){

	$.fn.mobileNav = function(options){

		// BEGIN: plagin options

		options = $.extend({
			selector: '.c-nav-mb',
			typeSelector: 'nav',
			navHeight: true,
			stopScroll: true
		}, options);

		// END: plagin options

		var $this 			= this,
			mainClass		= options.selector,
			typeSelector	= options.typeSelector,
			mainName		= mainClass.replace(/\./g, ""),
			navHeight		= options.navHeight,
			stopScroll		= options.stopScroll,
			$html			= $('html'),
			$body			= $('body'),
			winH,
			blockH;

		function headerContainerSize() {

		}

		function activeNav(event) {

			var crossClass = $this.attr('data-mobileNav-cross-class');

			if ( crossClass ) {
				unactiveNav(false, crossClass);
			}

			if ( !$body.hasClass( '_' + typeSelector + '_mb_active' ) ) {

				$body.addClass( '_' + typeSelector + '_mb_active' );

				if ( stopScroll ) {

					$body.addClass( '_' + typeSelector + '_mb_active_stop_scroll' );
				}

				event.stopImmediatePropagation();
			}
		}

		function unactiveNav(event, mainName) {

			if ( $body.hasClass( '_' + typeSelector + '_mb_active' ) ) {

				$body.removeClass( '_' + typeSelector + '_mb_active' );

				if ( stopScroll ) {

					$body.removeClass( '_' + typeSelector + '_mb_active_stop_scroll' );

				}

				if ( event ) {
					event.stopPropagation();
				}
			}
		}


		if ( navHeight ) {
			headerContainerSize();
		}

		$( mainClass + ', ' + mainClass + '__icon' ).click(function(event) {
			activeNav(event);
		});

		$( mainClass + '__container').click(function(event) {
			event.stopImmediatePropagation();
		});

		$('body, ' + mainClass + '__cross, ' + mainClass + ' .c-mobileNav-link').click(function(event) {
			unactiveNav(event, mainName);
		});

		$(window).resize(function(event) {

			if ( navHeight ) {
				headerContainerSize();
			}
		});

	};

})(jQuery);
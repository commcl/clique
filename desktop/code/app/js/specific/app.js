(function($) {

	function stickyMenuUpdate() {

		// libris js

		var offset  = $(window).scrollTop(),
			position = 0;

		if (offset > position) {

			$('.c-header').addClass('c-header_active');

		} else {

			$('.c-header').removeClass('c-header_active');
		}
	}



	$(document).ready(function() {

		stickyMenuUpdate();

		(function() {

			if ( $('.c-modal').length ) {

				$('.c-modal').modal();
			}
		})();

		(function() {

			if ( $('.c-scroll').length ) {

				$('.c-scroll').each(function(index, el) {

					$(this).perfectScrollbar({
						maxScrollbarLength: 160
					});
				});
			}
		})();

		(function() {

			if ( $('.c-scroll-to-next-block').length ) {

				$('.c-scroll-to-next-block').on('click', function(event) {

					event.preventDefault();

					$('body, html').animate({

						scrollTop: $('.c-scroll-to-next-block__bind').offset().top

					}, 200);

				});
			}
		})();

		(function() {

			if ( $('.c-slick_fade').length ) {

				$('.c-slick_fade').slick({
					autoplay: true,
					fade: true,
					speed: 300,
					slidesToShow: 1,

					arrows: false,
					dots: true,

					centerMode: true
				});
			}
		})();

		(function() {

			if ( $('.c-switcher').length ) {

				$('.c-switcher').each(function(index, el) {

					var $this = $(this);

					$this.on('click', function(event) {

						addingDataAttribute($this);
					});

					function addingDataAttribute($this) {
					
						if ( $this.data('c-switcher-class') && $this.data('c-switcher-element') ) {

							var element		= $this.data('c-switcher-element'),
								className	= $this.data('c-switcher-class');

							$(element).toggleClass(className);
						}
					};
				});
			}
		})();

		(function() {

			if ( $('.c-password-view').length ) {

				$('.c-password-view').each(function(index, el) {

					var $this	= $(this),
						number	= $this.data('c-password-view-number'),
						$bind	= $('.c-password-view__bind').filter('[data-c-password-view-number="' + number + '"]');

					$bind.on('click', function(event) {

						toggleClass($this);
					});

					function toggleClass($this) {
					
						if ( $this.attr('type') == 'text' ) {

							$this.attr('type', 'password')

						} else if ( $this.attr('type') == 'password' ) {

							$this.attr('type', 'text')
						}
					};
				});
			}
		})();

		(function() {

			if ( $('.c-switcher-hover').length ) {

				$('.c-switcher-hover').each(function(index, el) {

					var $this			= $(this),
						element			= $this.data('c-switcher-hover-element'),
						className		= $this.data('c-switcher-hover-class'),
						classElement	= $this.data('c-switcher-hover-class-element');

					$( element ).hover(

						function(){

							$( classElement + '[data-c-switcher-hover-number=' + $(this).data('c-switcher-hover-number') + ']' ).addClass( className );

						},

						function(){

							$( classElement + '[data-c-switcher-hover-number=' + $(this).data('c-switcher-hover-number') + ']' ).removeClass( className );
						}

					);
				});
			}
		})();

		(function() {

			if ( $('.c-typed').length ) {

				$('.c-typed').each(function(index, el) {

					var typed = new Typed(".c-typed", {
						stringsElement: '.c-typed__content',
						typeSpeed: 20,
						loop: false,
						cursorChar: '│'
					});

				});
			}
		})();
	});

	$(window).load(function() {

		(function() {

			if ( $('.c-nav-mb').length ) {

				var className = '.c-nav-mb';

				$(className).mobileNav({
					selector:		className
				});
			};
		})();

		(function() {

			if ( $('.c-checklist-mb').length ) {

				var className = '.c-checklist-mb';

				$(className).mobileNav({
					selector:		className,
					typeSelector:	'checklist'
				});
			};
		})();
	});

	$(window).on('touchmove scroll', function(){

		stickyMenuUpdate();
	});

})(jQuery);